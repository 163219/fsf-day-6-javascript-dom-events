/**
 * Created by TC on 5/4/16.
 */
var express = require("express");

var app = express();

app.use(express.static(__dirname + "/public"));

app.listen(3000, function(){
    console.info("Application started on port 3000");

})
